$(window).on('load', function () {
    var $preloader = $('#preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
    $('body').removeClass('loader');
});

$(document).ready(function() {


    // mask phone
    $("[name=phone]").mask("+7 (999) 999-9999");
    // mask phone


    // anchor
    $(".anchor").on("click","a", function (event) {
        event.preventDefault();
        var id  = $(this).attr('href'),
            top = $(id).offset().top;
        $('body,html').animate({scrollTop: top - 0}, 1000);
    });
    // anchor

    // slider
    $('.single-item').slick();
    // slider

    // animation iphone
    $('.programAnim').appear(function() {
        programAnim('.programAnim__item-1', '0', 2000);
        programAnim('.programAnim__item-2', '12%', 2000);
        programAnim('.programAnim__item-3', '32%', 2000);
        programAnim('.programAnim__item-4', '53%', 2000);
        programAnim('.programAnim__item-5', '64%', 2000);
    });
    function programAnim (item, itemTop, counter) {
        setTimeout(function() {
            $(item).animate({
                top: itemTop
            }, 2500);
        }, counter);
    }
    // animation iphone

    // sidemenu
    $('.mainMenuBtn').click(function (){
        $(this).parents().find('.mainMenu').css('left', '0');
    });

    $('.mainMenuClose').click(function (){
        $(this).parent().css('left', '-100%');
    });
    // sidemenu

    // animation number
    $('.number').appear(function() {
        $('.number').each(function(){
            var dataN = $(this).attr('data-number');
            $(this).animate({ num: dataN}, {
                duration: 3000,
                step: function (num){
                    this.innerHTML = (num).toFixed(0)
                }
            });
        });
    });

});

